# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.TextField()),
                ('created', models.DateTimeField(default=datetime.date(2014, 10, 5), auto_now_add=True)),
                ('changed', models.DateTimeField(default=datetime.date(2014, 10, 5), auto_now=True)),
                ('published', models.BooleanField(default=True)),
                ('autor', models.ForeignKey(related_name=b'comment autor', default=b'', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Posts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('body', models.TextField()),
                ('slug', models.SlugField(unique=True, max_length=100)),
                ('created', models.DateTimeField(default=datetime.date(2014, 10, 5), auto_now_add=True)),
                ('changed', models.DateTimeField(default=datetime.date(2014, 10, 5), auto_now=True)),
                ('published', models.BooleanField(default=True)),
                ('autor', models.ForeignKey(related_name=b'autor', default=b'', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='comments',
            name='post',
            field=models.ForeignKey(to='posts.Posts'),
            preserve_default=True,
        ),
    ]
