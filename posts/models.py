from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
import datetime

def _get_username(self):
	"""return username"""
	return User.objects.get(id = self.autor_id)

# Create your models here.
class Posts(models.Model):
	"""docstring for ClassName"""
	title = models.CharField(max_length=100)
	body = models.TextField()
	slug = models.SlugField(max_length=100, unique=True)
	autor = models.ForeignKey(User, related_name='autor', default='')
	created = models.DateTimeField(auto_now_add=True, default=datetime.date.today())
	changed = models.DateTimeField(auto_now=True, default=datetime.date.today())
	published = models.BooleanField(default=True)
	autorname = property(_get_username)

	class Meta:
		ordering = ['-created']
 
	def __unicode__(self):
		return u'%s' % self.title
 
	def get_absolute_url(self):
		return reverse('posts.views.posts', args=[self.slug])

class PostForm(ModelForm):
    class Meta:
        model = Posts
        fields = ['title', 'body', 'slug']

class Comments(models.Model):
	"""docstring for ClassName"""
	body = models.TextField()
	autor = models.ForeignKey(User, related_name='comment autor', default='')
	created = models.DateTimeField(auto_now_add=True, default=datetime.date.today())
	changed = models.DateTimeField(auto_now=True, default=datetime.date.today())
	published = models.BooleanField(default=True)
	post = models.ForeignKey(Posts)
	autorname = property(_get_username)

	class Meta:
		ordering = ['-created']
	
class CommentForm(ModelForm):
    class Meta:
        model = Comments
        fields = ['body']
